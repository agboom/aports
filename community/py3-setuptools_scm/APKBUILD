# Contributor: Dmitry Romanenko <dmitry@romanenko.in>
# Maintainer: Dmitry Romanenko <dmitry@romanenko.in>
pkgname=py3-setuptools_scm
_pkgname=setuptools_scm
pkgver=6.2.0
pkgrel=0
pkgdesc="The blessed package to manage your versions by scm tags"
url="https://github.com/pypa/setuptools_scm"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-setuptools py3-pip"
checkdepends="py3-pytest git"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver

replaces="py-setuptools_scm" # Backwards compatibility
provides="py-setuptools_scm=$pkgver-r$pkgrel" # Backwards compatibility

case "$CARCH" in
mips*)	options="!check" ;;
x86)	options="!check" ;; # hg is missing for x86
*)	checkdepends="$checkdepends mercurial" ;;
esac

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH=$PWD/src python3 -m pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
33c0a7c4be0df296dd859989f94db6eddf553e37e5580abe1dda63c33054e709a672a5eb9dc6f262f482f4d2a5507511aa0332022d46943251b5c522dff7c840  setuptools_scm-6.2.0.tar.gz
"
