# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Sergii Sadovyi <serg.sadovoi@gmail.com>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=terraform
pkgver=1.0.2
pkgrel=0
pkgdesc="Building, changing and combining infrastructure safely and efficiently"
url="https://www.terraform.io/"
arch="all"
license="MPL-2.0"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/terraform/archive/v$pkgver.tar.gz"
builddir="$srcdir/src/github.com/hashicorp/$pkgname"
options="chmod-clean"

prepare() {
	mkdir -p ${builddir%/*}
	mv $srcdir/$pkgname-$pkgver "$builddir"/
	default_prepare
}

export GOPATH="$srcdir"

build() {
	go build -v -o bin/$pkgname \
		-mod=readonly -ldflags "-X main.GitCommit=v$pkgver -X github.com/hashicorp/terraform/version.Prerelease= -s -w"
}

check() {
	case "$CARCH" in
		arm*|x86)
			go list -mod=readonly . | xargs -t -n4 \
			go test -mod=readonly -timeout=2m -parallel=4
			;;
		*) go test ./... ;;
	esac
	bin/$pkgname -v
}

package() {
	install -Dm755 "$builddir"/bin/$pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
d5d2db05d07e8f7266d58c9a99da86d0f67ee187a86ec467b5f4f107d75d78356e385a17221120f0c9d375389b77240de8026945fecba0bece6215c6fc544f04  terraform-1.0.2.tar.gz
"
